#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    //Checks to make sure there is input for our filename and our string that we are searching for
    //Also makes sure there isn't anything else in the array in case it messes anything up
    if(argv[1] == NULL || argv[2] == NULL || argv[3] != NULL)
    {
        printf("Error, please input a filename and string to search for seperated by a space\n");
    }
    else
    {
        //line counter to return what line the string was on
        int linecount = 1;
        
        //This is the string we are searching for
        char targetstring[255];
        strcpy(targetstring, argv[2]);
        
        //Opens and reads the file
        FILE *file = fopen(argv[1], "r");
        //Do code if there is a file, else return and error message
        if (file != NULL)
        {
            //Our line variable
            char line[512];
            while (fgets(line, sizeof(line), file) != NULL)
            {
                if(strstr(line, targetstring) != NULL)
                {
                    printf("Line %d: %s", linecount, line);
                }
                linecount++;
            }
            fclose(file);
        }
        else
        {
            printf("Error, could not find file\n");
        }
    }
}
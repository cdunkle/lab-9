#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "md5.h"

//Commands to use this in terminal
//clang makehash.c md5.c -o makehash -l crypto
//gcc makehash.c md5.c -o makehash -l crypto -o makehash
//./makehash testfile1.txt outputfile1.txt

int main(int argc, char *argv[])
{

    //This will make sure there is input in the two arguments that we need
    if(argv[1] == NULL || argv[2] == NULL || argv[3] != NULL)
    {
        printf("Please enter a filename to read from and a destination filename\n");
    }
    else
    {
        //Opens and reads the source file
        FILE *source_file = fopen(argv[1], "r");
        
        //Do code if there is a file, else return and error message
        if (source_file != NULL)
        {
            //Our line variable for each line in the source
            char line[512];
            
            //Loop through the source file and hash each line
            while (fgets(line, sizeof(line), source_file) != NULL)
            {
                //CUT OFF NEWLINE:
                for(int i = 0; i < strlen(line); i++)
                {
                    if(line[i] == '\n')
                    {
                        line[i] = '\0';
                    }
                }
                
                //HASH THE LINE HERE:
                char *hashedline = md5(line, strlen(line));
            
                //Add on a newline onto the end so that we can decode 
                //it later
                strcat(hashedline, "\n");
                
                //OPEN THE DESTINATION FILE:
                FILE *destination_file = fopen(argv[2], "a");
                //WRITE TO DESTINATION FILE:
                fputs(hashedline, destination_file);
                //CLOSE DESTINATION FILE:
                fclose(destination_file);
            }
            fclose(source_file);
        }
    }
}